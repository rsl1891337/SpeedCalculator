# SpeedCalculator



## About

This application can be used to find (consistent) speed tunes for several content areas in RSL. Not only can you show the outcome for a given team respecting the typical RNG factors but also check whether it is consistent, i.e. the outcome satisfies different criterias, and search for those teams.

## Getting started

Download the .exe installer and run it. If you have already installed the application, you might have to uninstall the existing application first.

## Help

If you have any questions, feedback or want to report a bug? Feel free to join the [Discord-Server](https://discord.gg/UMEn3SQPHb).

## Acknowledgement

This application wouldn't have been possible without the work of the already existing calculators. Thanks to all that put a lot of work in these projects!
I also want to thank everyone who tested the application and gave my some initial feedback, in particular [Bronko](https://www.youtube.com/@bronkoraidshadowlegends3768).
